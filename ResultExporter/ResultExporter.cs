﻿using System;
using System.IO;
using ResultExporter.Properties;

namespace ResultExporter
{
    public class ResultExporter : IDisposable
    {
        private static readonly string Separator = Settings.Default.ValuesSeparator ?? ";";

        #region Singleton
        public static ResultExporter Instance { get; private set; } = Create();
        #endregion
        private TextWriter OutputStream { get; set; }

        public void WriteLine(string result)
        {
            OutputStream.WriteLine(result);
        }

        public void WriteLine(params string[] values)
        {
            OutputStream.WriteLine(string.Join(Separator, values));
        }

        public void WriteLine()
        {
            OutputStream.WriteLine();
        }

        public void Write(string result)
        {
            OutputStream.Write(result);
        }

        public void Write(params string[] values)
        {
            OutputStream.Write(string.Join(Separator, values));
        }

        public void Close()
        {
            if (OutputStream == null) return;
            OutputStream.Close();
            OutputStream = null;
        }

        #region Factory Methods
        public static ResultExporter Create()
        {
            return Create($"results_{DateTime.Now:yy_MM_dd__HH_mm_ss}.csv");
        }

        public static ResultExporter Create(string outputFile)
        {
            return Create(new StreamWriter(outputFile));
        }

        public static ResultExporter Create(TextWriter outputStream)
        {
            return new ResultExporter { OutputStream = outputStream };
        }
        #endregion

        #region IDisposable
        public void Dispose()
        {
            if (OutputStream == null) return;
            try
            {
                OutputStream.Close();
            }
            catch (SystemException) { }
        }
        #endregion

    }
}
